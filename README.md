# Sample Thesis Document

A latex based sample document to be used for theses (e.g. PhD, Master, Bachelor).

## Example

See the compiled example: [latex-thesis-template.pdf](https://gitlab.com/nwaldispuehl/latex-thesis-template/raw/master/latex-thesis-template.pdf).

## How to prepare

Install latex on ubuntu:

    $ sudo apt-get install texlive-latex-base texlive-latex-recommended

## How to buid

To compile the ```tex``` document:

    $ pdflatex latex-thesis-template.tex
    $ bibtex latex-thesis-template.aux
    $ pdflatex latex-thesis-template.tex
    $ pdflatex latex-thesis-template.tex
